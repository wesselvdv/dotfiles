if OS.mac?
  tap 'koekeishiya/formulae'
  brew 'coreutils' # Those that come with macOS are outdated
  brew 'mas' # Mac App Store manager

  # Web Browsers
  cask 'brave-browser'
  cask 'firefox'

  # Tiling WM-related
  brew 'jq'
  brew 'yabai', restart_service: true
  brew 'koekeishiya/formulae/skhd', restart_service: true
  cask 'ubersicht'

  # Mac App Store
  mas 'Xcode', id: 497799835 # Requires an Apple ID.
  mas 'Amphetamine', id: 937984704
  mas '1Password', id: 1333542190
  mas 'The Unarchiver', id: 425424353
  mas 'WhatsApp', id: 1147396723 
  mas 'Rocket.Chat', id: 1086818840

  # Terminal Emulators
  cask 'alacritty'

  # Typefaces
  tap 'homebrew/cask-fonts'
  cask 'font-hack-nerd-font'
  
  # Quicklook
  cask 'qlcolorcode'
  cask 'qlmarkdown'
  cask 'quicklook-json'
  cask 'quicklook-csv'
  cask 'qlstephen'

  # Productivity
  cask 'alfred'
  cask 'docker'
  cask 'dropbox'
  cask 'discord'
  cask 'insomnia'
  cask 'tunnelblick'
  cask 'skype'
  cask 'slack'
  cask '1password-cli'
  cask 'microsoft-teams'
  cask 'intellij-idea'
  cask 'lens'

  tap 'xen0l/homebrew-taps'
  brew 'aws-gate'
end

# Taps
tap 'coinbase/assume-role'
tap 'jesseduffield/lazydocker'

# Node
tap 'nodenv/nodenv'
brew 'nodenv'
brew 'nodenv-package-json-engine'
brew 'nodenv-package-rehash'
brew 'nodenv-each'

# Python
brew 'pyenv'
brew 'pyenv-virtualenv'
brew 'pyenv-ccache'
brew 'pyenv-which-ext'

# Recommended for pyenv see: https://github.com/pyenv/pyenv/wiki
brew 'openssl'
brew 'readline'
brew 'sqlite3'
brew 'xz'
brew 'zlib'

# Ruby
brew 'rbenv'

# Golang
brew 'goenv'

# Binaries
brew 'bash'
brew 'git'
brew 'awscli'
brew 'tfenv'
brew 'awsebcli'
brew 'expect'
brew 'lazydocker'
brew 'lazygit'

brew 'vim'
brew 'neovim'
brew 'pgcli'

brew 'tmux'
brew 'zsh'
brew 'zplug'
brew 'exa'
brew 'gpg'
brew 'openstackclient'
brew 'yq'
brew 'kops'
brew 'kubernetes-cli'
brew 'helm'
brew 'starship'

