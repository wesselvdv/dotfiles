-r python-system-requirements.txt

msgpack==0.6.1
neovim==0.3.1
pynvim==0.4.0
jedi==0.14.1
setuptools==41.6.0

vim-vint==0.3.21
