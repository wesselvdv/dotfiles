ulimit -n 10240
ulimit -u 1024

export ZPLUG_HOME=/usr/local/opt/zplug
export LC_ALL=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8
export LANG=en_US.UTF-8
export PATH="/usr/local/sbin:${PATH}"
export PATH="/usr/local/opt/openssl@1.1/bin:$PATH"
export EDITOR="nvim"
export TMPDIR=/tmp

fpath+=${ZDOTDIR:-~}/.zsh_function.d
fpath+=${ZDOTDIR:-~}/.zsh_completion.d

source ${ZPLUG_HOME}/init.zsh

# zplug romkatv/powerlevel10k, as:theme, depth:1 
zplug sainnhe/dotfiles, use:.zsh-theme-gruvbox-material-light
zplug zdharma/fast-syntax-highlighting 

# Install plugins if there are plugins that have not been installed
if ! zplug check; then
  printf "Install? [y/N]: "
  if read -q; then
      echo; zplug install
  fi
fi

# Then, source plugins and add commands to $PATH
zplug load 

alias ls=exa

eval "$(starship init zsh)"

if which rbenv > /dev/null; then 
  eval "$(rbenv init -)" 
  export RBENV_ROOT=$(rbenv root)
fi

if which goenv > /dev/null; then 
  eval "$(goenv init -)" 
  export GOENV_ROOT=$(goenv root)
  export PATH="${GOROOT}/bin:${PATH}"
  export PATH="${PATH}:${GOPATH}/bin"
fi

if which pyenv > /dev/null; then
  eval "$(pyenv init -)" 
  export PYENV_ROOT=$(pyenv root)
fi

if which nodenv > /dev/null; then 
  eval "$(nodenv init -)" 
  export NODENV_ROOT=$(nodenv root)
fi

###-tns-completion-start-###
if [ -f /Users/wesselvdv/.tnsrc ]; then 
    source /Users/wesselvdv/.tnsrc 
fi
###-tns-completion-end-###

function run-gsc-toolbox() {
  export OS_USERNAME="$(1pass -p 'horizon - HS - openstack' username)"
  export OS_PASSWORD="$(1pass -p 'horizon - HS - openstack')"
  export IRN_USERNAME="$(1pass -p 'irn' username)"
  export IRN_PASSWORD="$(1pass -p 'irn')"

  cd /Users/isc79025/Projects/politie/gsc-as-code || exit 1
  ./runGscTenantToolbox.sh "${@}"
}
