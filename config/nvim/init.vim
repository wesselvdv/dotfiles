"                                _
"                         __   _(_)_ __ ___  _ __ ___
"                         \ \ / / | '_ ` _ \| '__/ __|
"                          \ V /| | | | | | | | | (__
"                         (_)_/ |_|_| |_| |_|_|  \___|
"

" General {{{
  " Indent
  set tabstop=2
  set shiftwidth=2
  set expandtab
  set list
  set listchars=tab:»-
  set re=1

  " Fold
  set foldlevel=100

  " Encoding
  set encoding=utf-8
  scriptencoding utf-8
  set fileencodings=utf-8,ucs-bom,iso-2022-jp-3,iso-2022-jp,eucjp-ms,euc-jisx0213,euc-jp,sjis,cp932

  " Font
  set guifont=Hack\ 11

  " Clipboard
  set clipboard=unnamed,unnamedplus

  " Search
  if has('nvim')
    set inccommand=split
  endif

  " Other
  set cursorline
  set title
  set number
  set noshowmode
  set hidden
" }}}

" Shougo/dein.vim {{{
  if &compatible
    set nocompatible
  endif

  set runtimepath+=~/.cache/dein/repos/github.com/Shougo/dein.vim

  if dein#load_state('~/.cache/dein')
    call dein#begin('~/.cache/dein')

    call dein#add('~/.cache/dein/repos/github.com/Shougo/dein.vim')

    " Appearance {{{
      call dein#add('vim-airline/vim-airline')
      call dein#add('vim-airline/vim-airline-themes')
      call dein#add('jeffkreeftmeijer/vim-numbertoggle')
      call dein#add('lifepillar/vim-solarized8')

      " Allow background opacity
      call dein#add('miyakogi/seiya.vim')
    " }}}

    " Tmux
    call dein#add('christoomey/vim-tmux-navigator')
    call dein#add('edkolev/tmuxline.vim')

    " Git
    call dein#add('tpope/vim-fugitive')

    " IDE
    call dein#add('neoclide/coc.nvim', {'merge':0, 'rev': 'release'})

    " Deferred {{{
      call dein#add('hashivim/vim-terraform', {'on_ft' : 'terraform'})
      call dein#add('fatih/vim-go', {'on_ft' : 'go'})

      call dein#add('leafgarland/typescript-vim', {'on_ft' : 'typescript'})
      call dein#add('peitalin/vim-jsx-typescript', {'on_ft' : 'typescript'})

      call dein#add('jparise/vim-graphql', {'on_ft' : 'graphql'})
    " }}}

    call dein#end()
    call dein#save_state()
  endif

  if dein#check_install()
    call dein#install()
  endif

  filetype plugin indent on
  syntax enable

  let g:dein#install_process_timeout = 300
" }}}

" lifepillar/vim-solarized8 {{{
  set bg=light
  set termguicolors
  colorscheme solarized8_high
" }}}

" miyakogi/seiya.vim {{{
  let g:seiya_auto_enable = 1
  let g:seiya_target_groups = has('nvim') ? ['guibg'] : ['ctermbg']
" }}}

" vim-airline/vim-airline {{{
  let g:airline_theme='solarized'
  let g:airline_powerline_fonts = 1
  set laststatus=2
  " Show branch name
  let g:airline#extensions#branch#enabled = 1
  " Show buffer's filename
  let g:airline#extensions#tabline#enabled = 1
  let g:airline#extensions#tabline#fnamemod = ':t'
  let g:airline#extensions#tabline#buffer_nr_show = 1
  let g:airline#extensions#wordcount#enabled = 0
  let g:airline#extensions#default#layout = [['a', 'b', 'c'], ['x', 'y', 'z']]
  let g:airline_section_c = '%t'
  let g:airline_section_x = '%{&filetype}'
  "let g:airline_section_z = '%3l:%2v %{airline#extensions#ale#get_warning()} %{airline#extensions#ale#get_error()}'
  let g:airline#extensions#ale#error_symbol = ' '
  let g:airline#extensions#ale#warning_symbol = ' '
  let g:airline#extensions#default#section_truncate_width = {}
  " Check whitespace at end of line
  let g:airline#extensions#whitespace#enabled = 1
" }}}

" PyEnv {{{
  if isdirectory($PYENV_ROOT.'/versions/neovim2')
    let g:python_host_prog = $PYENV_ROOT.'/versions/neovim2/bin/python'
  endif

  if isdirectory($PYENV_ROOT.'/versions/neovim3')
    let g:python3_host_prog = $PYENV_ROOT.'/versions/neovim3/bin/python'
  endif
" }}}

" neoclide/coc.vim {{{
  " Some servers have issues with backup files, see #649
  set nobackup
  set nowritebackup

  " Better display for messages
  set cmdheight=2

  " You will have bad experience for diagnostic messages when it's default 4000.
  set updatetime=300

  " don't give |ins-completion-menu| messages.
  set shortmess+=c

  " always show signcolumns
  set signcolumn=yes

  " Use tab for trigger completion with characters ahead and navigate.
  " Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
  inoremap <silent><expr> <TAB>
    \ pumvisible() ? "\<C-n>" :
    \ <SID>check_back_space() ? "\<TAB>" :
    \ coc#refresh()
  inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

  function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~# '\s'
  endfunction

  " Use <c-space> to trigger completion.
  inoremap <silent><expr> <c-space> coc#refresh()

  " Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
  " Coc only does snippet and additional edit on confirm.
  inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
  " Or use `complete_info` if your vim support it, like:
  " inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"

  " Use `[g` and `]g` to navigate diagnostics
  nmap <silent> [g <Plug>(coc-diagnostic-prev)
  nmap <silent> ]g <Plug>(coc-diagnostic-next)

  " Remap keys for gotos
  nmap <silent> gd <Plug>(coc-definition)
  nmap <silent> gy <Plug>(coc-type-definition)
  nmap <silent> gi <Plug>(coc-implementation)
  nmap <silent> gr <Plug>(coc-references)

  " Use K to show documentation in preview window
  nnoremap <silent> K :call <SID>show_documentation()<CR>

  function! s:show_documentation()
    if (index(['vim','help'], &filetype) >= 0)
      execute 'h '.expand('<cword>')
    else
      call CocAction('doHover')
    endif
  endfunction

  " Highlight symbol under cursor on CursorHold
  autocmd CursorHold * silent call CocActionAsync('highlight')

  " Remap for rename current word
  nmap <leader>rn <Plug>(coc-rename)

  " Remap for format selected region
  xmap <leader>f  <Plug>(coc-format-selected)
  nmap <leader>f  <Plug>(coc-format-selected)

  augroup mygroup
    autocmd!
    " Setup formatexpr specified filetype(s).
    autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
    " Update signature help on jump placeholder
    autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
  augroup end

  " Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
  xmap <leader>a  <Plug>(coc-codeaction-selected)
  nmap <leader>a  <Plug>(coc-codeaction-selected)

  " Remap for do codeAction of current line
  nmap <leader>ac  <Plug>(coc-codeaction)
  " Fix autofix problem of current line
  nmap <leader>qf  <Plug>(coc-fix-current)

  " Create mappings for function text object, requires document symbols feature of languageserver.
  xmap if <Plug>(coc-funcobj-i)
  xmap af <Plug>(coc-funcobj-a)
  omap if <Plug>(coc-funcobj-i)
  omap af <Plug>(coc-funcobj-a)

  " Use <C-d> for select selections ranges, needs server support, like: coc-tsserver, coc-python
  nmap <silent> <C-d> <Plug>(coc-range-select)
  xmap <silent> <C-d> <Plug>(coc-range-select)

  " Use `:Format` to format current buffer
  command! -nargs=0 Format :call CocAction('format')

  " Use `:Fold` to fold current buffer
  command! -nargs=? Fold :call     CocAction('fold', <f-args>)

  " use `:OR` for organize import of current buffer
  command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

  " Add status line support, for integration with other plugin, checkout `:h coc-status`
  set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

  " Using CocList
  " Show all diagnostics
  nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
  " Manage extensions
  nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
  " Show commands
  nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
  " Find symbol of current document
  nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
  " Search workspace symbols
  nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
  " Do default action for next item.
  nnoremap <silent> <space>j  :<C-u>CocNext<CR>
  " Do default action for previous item.
  nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
  " Resume latest coc list
  nnoremap <silent> <space>p  :<C-u>CocListResume<CR>

  " Remap keys for gotos
  nmap <silent> gd <Plug>(coc-definition)
  nmap <silent> gy <Plug>(coc-type-definition)
  nmap <silent> gi <Plug>(coc-implementation)
  nmap <silent> gr <Plug>(coc-references)

  " Golang {{{
    " Run format on save
    autocmd BufWritePre *.go :call CocAction('runCommand', 'editor.action.organizeImport')
  " }}}

  let g:coc_global_extensions = [
    \ 'coc-yaml',
    \ 'coc-vetur',
    \ 'coc-prettier',
    \ 'coc-angular',
    \ 'coc-json',
    \ 'coc-css',
    \ 'coc-sql',
    \ 'coc-tsserver'
  \]
" }}}

" fatih/vim-go {{{
    " disable vim-go :GoDef short cut (gd)
    " this is handled by LanguageClient [LC]
    let g:go_def_mapping_enabled = 0
" }}}

" hashivim/terraform {{{
  let g:terraform_align=1
  let g:terraform_fmt_on_save=1
  let g:terraform_fold_sections=1
" }}}

" File Types {{{
  augroup vimrc_filetype
    autocmd!
    autocmd BufRead,BufNewFile *.md set filetype=markdown
    autocmd BufRead,BufNewFile *.rc set filetype=sh
    autocmd BufRead,BufNewFile *.jenkinsfile set filetype=groovy
    autocmd BufRead,BufNewFile *.css,*.scss,*.less setlocal foldmethod=marker foldmarker={,}

    autocmd FileType go :highlight goErr ctermfg=208
    autocmd FileType go :match goErr /\<err\>/
    autocmd FileType html setlocal shiftwidth=2 tabstop=2
    autocmd FileType javascript setlocal shiftwidth=2 tabstop=2
    autocmd FileType json setlocal shiftwidth=2 tabstop=2
    autocmd FileType json syntax match Comment +\/\/.\+$+
    autocmd FileType ruby setlocal shiftwidth=2 tabstop=2
    autocmd FileType vim setlocal shiftwidth=2 tabstop=2 foldmethod=marker
    autocmd FileType vim :highlight link FoldComment SpecialComment
    autocmd FileType vim :match FoldComment /^".*\({{{\|}}}\)/
    autocmd FileType vue setlocal shiftwidth=2 tabstop=2
    autocmd FileType vue syntax sync fromstart
    autocmd FileType xml setlocal shiftwidth=2 tabstop=2
    autocmd FileType yaml setlocal shiftwidth=2 tabstop=2
    autocmd FileType zsh setlocal foldmethod=marker
    autocmd FileType zsh :highlight link FoldComment SpecialComment
    autocmd FileType zsh :match FoldComment /^#.*\({{{\|}}}\)/
  augroup END
" }}}

" Generic Keymaps {{{
  " Unhighlight search result
  nnoremap <Esc><Esc> :<C-u>nohlsearch<CR>
" }}}

" Fixes {{{

  " SQLComplete Error:
  let g:omni_sql_no_default_maps=1

" }}}
